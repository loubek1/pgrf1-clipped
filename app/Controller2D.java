package app;

import clip.Clipper;
import fill.ScanLine;
import fill.SeedFill;
import fill.SeedFillBorder;
import model.Circle;
import model.Line;
import model.Point;
import model.Polygon;
import rasterize.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu, ovladani mysi
 *
 * @author PGRF FIM UHK
 * @version 2020
 */
public class Controller2D {

    private JPanel panel;
    private RasterBufferedImage raster;
    private Point startPoint = null;
    private boolean isMouseDown = false;
    private Polygon polygon = new Polygon();
    private Polygon polygon2 = new Polygon();
    private Circle circle;
    private Line line;
    private Polygon clipped;


    private int drawingMode = 0; //0 - znaci polygon, 1 znaci pravouhly trojuhelnik
    private int seedFillMode = 0;
    private int poligonMode = 0;

    LineRasterizer mainRasterizer;
    LineRasterizer secondaryRasterizer;
    PolygonRasterizer polygonRasterizer;
    CircleRasterizer circleRasterizer;
    SeedFill seedFill;
    SeedFillBorder seedFillBorder;


    public Controller2D(int width, int height) {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferedImage(width, height);
        mainRasterizer = new FilledLineRasterizer(raster);
        secondaryRasterizer = new DottedLineRasterizer(raster);
        polygonRasterizer = new PolygonRasterizer(mainRasterizer, secondaryRasterizer);
        circleRasterizer = new CircleRasterizer(raster, Color.red);
        seedFill = null;
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    isMouseDown = true;
                    Point p = new Point(e.getX(), e.getY());

                    if (drawingMode == 0) {
                        if (poligonMode == 0) {
                            if (polygon.getPointsCount() == 0) {
                                polygon.addPoint(p);
                            }
                            polygon.addPoint(p);
                        }
                        if (poligonMode == 1) {
                            if (polygon2.getPointsCount() == 0) {
                                polygon2.addPoint(p);
                            }
                            polygon2.addPoint(p);
                        }
                    } else if (drawingMode == 1) {
                        if (startPoint == null)
                            startPoint = p;
                        if (circle != null) {
                            polygon = new Polygon();
                            polygon.addPoint(new Point(line.getX1(), line.getY1()));
                            polygon.addPoint(new Point(line.getX2(), line.getY2()));

                            //algo for shortest code
                            double vX = p.getX() - circle.getCenter().getX();
                            double vY = p.getY() - circle.getCenter().getY();
                            double magV = Math.sqrt(vX * vX + vY * vY);
                            double aX = circle.getCenter().getX() + vX / magV * circle.getRadius();
                            double aY = circle.getCenter().getY() + vY / magV * circle.getRadius();

                            polygon.addPoint(new Point(aX, aY));
                        }
                    }
                }

                if (e.getButton() == MouseEvent.BUTTON3) {
                    if (seedFillMode == 1) {
                        seedFillBorder = new SeedFillBorder(raster, 0xffff00, Color.black.getRGB(), e.getX(), e.getY());
                    } else {
                        seedFill = new SeedFill(raster, 0xffff00, Color.black.getRGB(), e.getX(), e.getY());
                    }

                }

            }

            @Override
            public void mouseReleased(MouseEvent e) {

                isMouseDown = false;
                Point p = new Point(e.getX(), e.getY());

                if (drawingMode == 1) {
                    if (circle == null) {
                        circle = new Circle(
                                new Point((startPoint.getX() + p.getX()) / 2, (startPoint.getY() + p.getY()) / 2),
                                (float) Math.sqrt(Math.pow(startPoint.getX() - p.getX(), 2) + Math.pow(startPoint.getY() - p.getY(), 2)) / 2);
                    }
                }
                panel.repaint();
            }
        });
        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (isMouseDown) {

                    Point p = new Point(e.getX(), e.getY());

                    if (drawingMode == 0) {
                        if (poligonMode == 0) {
                            polygon.replaceLastPoint(new Point(e.getX(), e.getY()));
                        }
                        if (poligonMode == 1) {
                            polygon2.replaceLastPoint(new Point(e.getX(), e.getY()));
                        }
                    } else if (drawingMode == 1) {
                        if (circle == null)
                            line = new Line(startPoint, p, 0xFF0000);
                        if (circle != null) {

                            //algo for shortest code
                            double vX = p.getX() - circle.getCenter().getX();
                            double vY = p.getY() - circle.getCenter().getY();
                            double magV = Math.sqrt(vX * vX + vY * vY);
                            double aX = circle.getCenter().getX() + vX / magV * circle.getRadius();
                            double aY = circle.getCenter().getY() + vY / magV * circle.getRadius();

                            polygon.replaceLastPoint(new Point(aX, aY));
                        }
                    }

                }

                panel.repaint();
            }
        });

        panel.requestFocus();
        panel.requestFocusInWindow();
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    polygon = new Polygon();
                    polygon2 = new Polygon();
                    seedFill = null;
                    seedFillBorder = null;
                    circle = null;
                    line = null;
                    isMouseDown = false;
                    clear();
                    panel.repaint();
                } else if (e.getKeyCode() == KeyEvent.VK_T) {
                    if (drawingMode == 0)
                        drawingMode = 1;
                    else
                        drawingMode = 0;
                    seedFill = null;
                    seedFillBorder = null;
                    polygon = new Polygon();
                    circle = null;
                    isMouseDown = false;
                    clear();
                    panel.repaint();
                } else if (e.getKeyCode() == KeyEvent.VK_F) {


                    circle = null;
                    line = null;
                    isMouseDown = false;
                    clear();
                    panel.repaint();
                    if (seedFillMode == 0){
                        seedFill = null;
                        seedFillMode = 1;}
                    else{
                        seedFillBorder = null;
                        seedFillMode = 0;
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_P) {
                    seedFill = null;
                    seedFillBorder = null;
                    if (poligonMode == 0)
                        poligonMode = 1;
                    else
                        poligonMode = 0;
                }else if (e.getKeyCode() == KeyEvent.VK_L) {
                    System.out.println("polygon1:");
                    for(int i= 0; polygon.getPoints().size()>i;i++){
                        Point point = polygon.getPoint(i);
                        System.out.println("x: " + point.getX());
                        System.out.println("y: " + point.getY());
                    }
                    System.out.println("polygon2:");
                    for(int i= 0; polygon2.getPoints().size()>i;i++){
                        Point point = polygon2.getPoint(i);
                        System.out.println("x: " + point.getX());
                        System.out.println("y: " + point.getY());
                    }
                    System.out.println("clipped:");
                    for(int i= 0; clipped.getPoints().size()>i;i++){
                        Point point = clipped.getPoint(i);
                        System.out.println("x: " + point.getX());
                        System.out.println("y: " + point.getY());
                    }

                }




            }
        });
    }

    public void present(Graphics graphics) {
        clear();

        if (drawingMode == 0) {
            polygonRasterizer.rasterize(polygon, isMouseDown, 0xFF0000);
            polygonRasterizer.rasterize(polygon2, isMouseDown, 0x00FF00);




            if (seedFill != null) {
                seedFill.fill();
            }
            if (seedFillBorder != null) {
                seedFillBorder.fill();
            }
        } else if (drawingMode == 1) {
            if (line != null)
                mainRasterizer.rasterize(line);
            if (circle != null) {
                circleRasterizer.rasterize(circle);
            }
            if (polygon != null)
                polygonRasterizer.rasterize(polygon, false, 0xFF0000);
        }

        clipped = Clipper.clip(polygon, polygon2);
        polygonRasterizer.rasterize(clipped, isMouseDown,0x00ffff);

        raster.repaint(graphics);
    }



    public void clear() {
        raster.clear();
    }

    public void start() {
        clear();

    }
}
