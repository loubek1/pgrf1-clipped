package clip;

import java.util.Iterator;
import model.Point;
import model.Polygon;

public class Clipper {
    public Clipper() {
    }

    public static Polygon clip(Polygon poly, Polygon clipPoly) {
        if (clipPoly.getPointsCount() < 2) {
            return poly;
        } else {
            Polygon newPoly = poly;
            Point p1 = clipPoly.getPoint(clipPoly.getPointsCount() - 1);

            Point p2;
            for(Iterator var4 = clipPoly.getPoints().iterator(); var4.hasNext(); p1 = p2) {
                p2 = (Point)var4.next();
                newPoly = clipEdge(poly, new Clipper.Edge(p1, p2));
                poly = newPoly;
            }

            return newPoly;
        }
    }

    private static Polygon clipEdge(Polygon poly, Clipper.Edge e) {
        if (poly.getPointsCount() < 2) {
            return poly;
        } else {
            Polygon out = new Polygon();
            Point v1 = poly.getPoint(poly.getPointsCount() - 1);

            Point v2;
            for(Iterator var4 = poly.getPoints().iterator(); var4.hasNext(); v1 = v2) {
                v2 = (Point)var4.next();
                if (e.inside(v2)) {
                    if (!e.inside(v1)) {
                        out.addPoint(e.intersection(v1, v2));
                    }

                    out.addPoint(v2);
                } else if (e.inside(v1)) {
                    out.addPoint(e.intersection(v1, v2));
                }
            }

            return out;
        }
    }

    public static class Edge {
        Point p1;
        Point p2;

        public Edge(Point p1, Point p2) {
            this.p1 = p1;
            this.p2 = p2;
        }

        public boolean inside(Point p) {
            Point v1 = new Point(this.p2.getX() - this.p1.getX(), this.p2.getY() - this.p1.getY());
            Point n1 = new Point(-v1.getY(), v1.getX());
            Point v2 = new Point(p.getX() - this.p1.getX(), p.getY() - this.p1.getY());
            return (double)(n1.getX() * v2.getX() + n1.getY() * v2.getY()) < 0.0D;
        }

        public Point intersection(Point v1, Point v2) {
            int l1_x1 = v1.getX();
            int l1_y1 = v1.getY();
            int l1_x2 = v2.getX();
            int l1_y2 = v2.getY();
            int l2_x1 = this.p1.getX();
            int l2_y1 = this.p1.getY();
            int l2_x2 = this.p2.getX();
            int l2_y2 = this.p2.getY();
            float px = (float)(((l1_x1 * l1_y2 - l1_y1 * l1_x2) * (l2_x1 - l2_x2) - (l2_x1 * l2_y2 - l2_y1 * l2_x2) * (l1_x1 - l1_x2)) / ((l1_x1 - l1_x2) * (l2_y1 - l2_y2) - (l2_x1 - l2_x2) * (l1_y1 - l1_y2)));
            float py = (float)(((l1_x1 * l1_y2 - l1_y1 * l1_x2) * (l2_y1 - l2_y2) - (l2_x1 * l2_y2 - l2_y1 * l2_x2) * (l1_y1 - l1_y2)) / ((l1_x1 - l1_x2) * (l2_y1 - l2_y2) - (l2_x1 - l2_x2) * (l1_y1 - l1_y2)));
            return new Point((int)px, (int)py);
        }
    }
}
