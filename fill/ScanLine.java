package fill;

import model.Edge;
import model.Point;
import model.Polygon;
import rasterize.Raster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ScanLine implements Filler {

    private int fillColor, borderColor;
    private Raster raster;

    private List<Point> vertices;
    private Polygon poly;
    private Polygon clipPoly;


    public ScanLine(int fillColor, int borderColor, Raster raster,Polygon poly, Polygon clipPoly) {
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.raster = raster;
        this.clipPoly = clipPoly;
        this.poly = poly;
    }

    @Override
    public void fill() {



        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < vertices.size(); i++) {
            Point startVertex = vertices.get(i);
            Point endVertex = vertices.get((i + 1) % vertices.size());
            if (startVertex.getY() == endVertex.getY())
                continue;

            Edge edge = new Edge(startVertex, endVertex);
            edge.orientate();
            edges.add(edge);
        }

        int yMin = vertices.get(0).getY();
        int yMax = yMin;
        for (int i = 0; i < vertices.size(); i++) {
            var vertex = vertices.get(i);
            if (vertex.getY() > yMax)
                yMax = vertex.getX();
            if (vertex.getY() < yMin)
                yMin = vertex.getY();
        }


        for (int y = yMin; y <= yMax; y++) {
            var intersections = new ArrayList<Integer>();
            for (Edge edge : edges) {
                if (edge.getY1() <= y && edge.getY2() > y) {
                    float k = ((edge.getX2() - edge.getX1()) / (float) (edge.getY2() - edge.getY1()));
                    float q = edge.getX1() - k * edge.getY1();
                    var intersection = k * y + q;
                    intersections.add((int) intersection);
                }
            }

            Collections.sort(intersections);
            for (int i = 0; i < intersections.size(); i += 2) {
                if (i + 1 > intersections.size() - 1)
                    break;

                var start = intersections.get(i);
                var end = intersections.get(i + 1);
                for (int x = start; x < end; x++) {
                    if (clipPoly.containsPoint(new Point(x, y))) {
                        raster.setPixel(x,y,fillColor);
                    }
                }
            }

        }
    }
}
