package fill;

import rasterize.Raster;

public class SeedFillBorder implements Filler {
    private Raster raster;
    private int fillColor, backgroundColor;

    private int x, y;

    public SeedFillBorder(Raster raster, int fillColor, int backgroundColor, int x, int y) {
        this.raster = raster;
        this.fillColor = fillColor;
        this.backgroundColor = backgroundColor;
        this.x = x;
        this.y = y;
    }

    @Override
    public void fill() {
        seedFill(x, y);
    }

    private void seedFill(int x, int y) {
        if (raster.getPixel(x, y) == backgroundColor) {


            raster.setPixel(x, y, fillColor);
            seedFill(x + 1, y);
            seedFill(x - 1, y);
            seedFill(x, y + 1);
            seedFill(x, y - 1);

        }
       else if (raster.getPixel(x, y) != backgroundColor) {

            raster.setPixel(x, y, fillColor);
       }
}
}
