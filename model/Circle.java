package model;

public class Circle {

    private Point center;
    private float radius;


    public Circle(Point center, float diameter) {
        this.center = center;
        this.radius = diameter;
    }

    public Point getCenter() {
        return center;
    }

    public float getRadius() {
        return radius;
    }
}
