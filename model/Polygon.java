package model;

import java.util.ArrayList;
import java.util.List;

public class Polygon {
    List<Point> points;

    public Polygon() {
        this.points = new ArrayList<>();
    }

    public void addPoint(Point point) {
        this.points.add(point);
    }

    public void replaceLastPoint(Point p) {
        this.points.remove(getPointsCount() - 1);
        this.points.add(p);
    }

    public boolean containsPoint(Point p) {
        var x = p.getX();
        var y = p.getY();
        var result = false;

        for (int i = 0, j = points.size() - 1; i < points.size(); j = i++) {
            var p1 = points.get(i);
            var p2 = points.get(j);

            if ((((p1.getY() <= y) && (y < p2.getY())) ||
                    ((p2.getY() <= y) && (y < p1.getY()))) &&
                    (x < (p2.getX() - p1.getX()) * (y - p1.getY()) / (p2.getY() - p1.getY()) + p1.getX()))
                result = !result;
        }
        return result;
    }

    public int getPointsCount() {
        return points.size();
    }

    public Point getPoint(int i) {
        return points.get(i);
    }

    public List<Point> getPoints() {
        return points;
    }
}
