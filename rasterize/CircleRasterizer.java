package rasterize;

import model.Circle;

import java.awt.*;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class CircleRasterizer {

    private Raster raster;
    private Color color;

    public CircleRasterizer(Raster raster, Color color){
        this.raster = raster;
        this.color = color;
    }

    public void rasterize(Circle circle) {

        double x, y;
        float eps = 0.001f;

        for (float fi = 0; fi <= Math.PI / 2; fi += eps) {
            x = circle.getRadius() * cos(fi);
            y = circle.getRadius() * sin(fi);

            raster.setPixel((int)Math.round(x) + circle.getCenter().getX(),
                    (int) Math.round(y) + circle.getCenter().getY(), this.color.getRGB());
            raster.setPixel((int)Math.round(-x) + circle.getCenter().getX(),
                    (int) Math.round(y) + circle.getCenter().getY(), this.color.getRGB());
            raster.setPixel((int)Math.round(x) + circle.getCenter().getX(),
                    (int) Math.round(-y) + circle.getCenter().getY(), this.color.getRGB());
            raster.setPixel((int)Math.round(-x) + circle.getCenter().getX(),
                    (int) Math.round(-y) + circle.getCenter().getY(), this.color.getRGB());
        }

    }

}
