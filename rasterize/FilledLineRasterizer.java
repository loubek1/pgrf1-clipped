package rasterize;

import static java.lang.Math.abs;

//Triviální algoritmus

public class FilledLineRasterizer extends LineRasterizer {

    public FilledLineRasterizer(Raster raster) {
        super(raster);
    }

    @Override
    protected void drawLine(int x1, int y1, int x2, int y2) {
        int dx = x2 - x1;
        int dy = y2 - y1;

        float k = dy / (float) dx;
        float q = y1 - k * x1;

        if (abs(dy) < abs(dx)) {
            if (x2 < x1) {
                int tmp = x1;
                x1 = x2;
                x2 = tmp;
            }

            for (int x = x1; x <= x2; x++) {
                float y = k * x + q;

                raster.setPixel(x, Math.round(y), this.color.getRGB());

            }
        } else {
            if (y1 > y2) {
                int tmp = y1;
                y1 = y2;
                y2 = tmp;
            }

            for (int y = y1; y <= y2; y++) {
                float x = (y - q) / k;
                if (x1 == x2) {
                    x = x2;
                }

                raster.setPixel(Math.round(x), y, this.color.getRGB());
            }
        }
    }
}




