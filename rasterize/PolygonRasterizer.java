package rasterize;

import model.Line;
import model.Polygon;

public class PolygonRasterizer {
    private final LineRasterizer mainRasterizer;
    private LineRasterizer secondaryRasterizer;

    public PolygonRasterizer(LineRasterizer mainRasterizer, LineRasterizer secondaryRasterizer) {
        this.mainRasterizer = mainRasterizer;
        this.secondaryRasterizer = secondaryRasterizer;
    }

    public void rasterize(Polygon polygon, boolean isMouseDown, int color) {
        if(polygon.getPointsCount() < 2)
            return;

        LineRasterizer rasterizer = mainRasterizer;

        for (int i = 0; i < polygon.getPointsCount() - 1; i++) {

            if(i == polygon.getPointsCount() - 2 && isMouseDown)
                rasterizer = secondaryRasterizer;

            rasterizer.rasterize(new Line(polygon.getPoint(i),polygon.getPoint(i + 1),color));
        }

        rasterizer.rasterize(new Line(polygon.getPoint(0),polygon.getPoint(polygon.getPointsCount() - 1),color));

    }
}
